use anyhow::{anyhow, bail, Result};
use oauth2::{
    basic::{BasicErrorResponseType, BasicTokenType},
    RevocationErrorResponseType, StandardRevocableToken,
};
use openidconnect::{
    core::{
        CoreAuthDisplay, CoreAuthPrompt, CoreAuthenticationFlow, CoreClient, CoreGenderClaim,
        CoreJsonWebKey, CoreJsonWebKeyType, CoreJsonWebKeyUse, CoreJweContentEncryptionAlgorithm,
        CoreJwsSigningAlgorithm, CoreProviderMetadata,
    },
    reqwest::http_client,
    AccessTokenHash, AuthorizationCode, Client, ClientId, ClientSecret, CsrfToken,
    EmptyAdditionalClaims, EmptyExtraTokenFields, IdTokenFields, IssuerUrl, Nonce,
    OAuth2TokenResponse, PkceCodeChallenge, PkceCodeVerifier, RedirectUrl, Scope,
    StandardErrorResponse, StandardTokenIntrospectionResponse, StandardTokenResponse,
    TokenResponse,
};

use serde::{Deserialize, Serialize};
use url::Url;

type InnerClient = Client<
    EmptyAdditionalClaims,
    CoreAuthDisplay,
    CoreGenderClaim,
    CoreJweContentEncryptionAlgorithm,
    CoreJwsSigningAlgorithm,
    CoreJsonWebKeyType,
    CoreJsonWebKeyUse,
    CoreJsonWebKey,
    CoreAuthPrompt,
    StandardErrorResponse<BasicErrorResponseType>,
    StandardTokenResponse<
        IdTokenFields<
            EmptyAdditionalClaims,
            EmptyExtraTokenFields,
            CoreGenderClaim,
            CoreJweContentEncryptionAlgorithm,
            CoreJwsSigningAlgorithm,
            CoreJsonWebKeyType,
        >,
        BasicTokenType,
    >,
    BasicTokenType,
    StandardTokenIntrospectionResponse<EmptyExtraTokenFields, BasicTokenType>,
    StandardRevocableToken,
    StandardErrorResponse<RevocationErrorResponseType>,
>;

pub(super) struct OIDCClient {
    client: InnerClient,
}

impl OIDCClient {
    /// Build new OIdC client
    pub(super) fn new(
        issuer_url: &str,
        client_id: &str,
        client_secret: Option<&str>,
        redirect_uri: &str,
    ) -> Result<OIDCClient> {
        let provider_metadata =
            CoreProviderMetadata::discover(&IssuerUrl::new(issuer_url.to_string())?, http_client)?;
        let client_id = ClientId::new(client_id.to_string());
        let client_secret = client_secret.map(|secret| ClientSecret::new(secret.to_string()));
        let redirect_uri = RedirectUrl::new(redirect_uri.to_string())?;
        let client =
            CoreClient::from_provider_metadata(provider_metadata, client_id, client_secret)
                .set_redirect_uri(redirect_uri);
        Ok(OIDCClient { client })
    }

    /// Generate redirect url for user-agent
    pub(super) fn authorization_url(&self, scopes: &[&str]) -> Result<(Url, String)> {
        let (pkce_code_challenge, pkce_code_verifier) = PkceCodeChallenge::new_random_sha256();
        let (authentication_url, csrf_token, nonce) = self
            .client
            .authorize_url(
                CoreAuthenticationFlow::AuthorizationCode,
                CsrfToken::new_random,
                Nonce::new_random,
            )
            .add_scopes(scopes.iter().map(|scope| Scope::new(scope.to_string())))
            .set_pkce_challenge(pkce_code_challenge)
            .url();
        let security_details = SecurityDetails::new(csrf_token, nonce, pkce_code_verifier)?;
        let cookie_values = serde_json::to_string(&security_details)?;
        Ok((authentication_url, cookie_values))
    }

    /// Lets OpenID provider validate the authentication request; upon a successful validation, the
    /// provider returns among others an [access
    /// token](https://datatracker.ietf.org/doc/html/rfc6749#section-1.4) and an [ID
    /// token](https://openid.net/specs/openid-connect-core-1_0.html#IDToken) (which is specific
    /// for the OIDC standard. For the later use in an API context, the former is relevant and is
    /// thus returned by the function.
    pub(super) fn access_token(
        &self,
        authorization_code: &str,
        cookie_value: &str,
        state_param: &str,
    ) -> Result<String> {
        let authorization_code = AuthorizationCode::new(authorization_code.to_string());
        let security_details: SecurityDetails = serde_json::from_str(&cookie_value)?;
        if state_param != security_details.csrf_token.secret() {
            bail!("CSRF token validation failed");
        }
        let token_response = self
            .client
            .exchange_code(authorization_code)
            .set_pkce_verifier(security_details.pkce_code_verifier)
            .request(http_client)?;
        let id_token = token_response
            .id_token()
            .ok_or_else(|| anyhow!("Server did not return an ID token"))?;
        let claims = id_token.claims(&self.client.id_token_verifier(), &security_details.nonce)?;
        // Verify the access token hash to ensure that the access token hasn't benn substituted for
        // another user's
        if let Some(expected_access_token_hash) = claims.access_token_hash() {
            let actual_access_token_hash = AccessTokenHash::from_token(
                token_response.access_token(),
                &id_token.signing_alg()?,
            )?;
            if actual_access_token_hash != *expected_access_token_hash {
                bail!("Invalid access token");
            }
        }
        serde_json::to_string(token_response.access_token()).map_err(|e| e.into())
    }

    pub(super) fn refresh_token(&self) {
        // self.client.exchange_refresh_token()
        todo!()
    }
}

#[derive(Deserialize, Serialize)]
pub(super) struct SecurityDetails {
    /// The CSRF token serves as a safeguard against a so-called cross-site request forgery, where
    /// unauthorised commands are submitted unknowingly from a user that the web server application
    /// trusts and grants privileged access.
    csrf_token: CsrfToken,
    ///
    nonce: Nonce,
    ///
    pkce_code_verifier: PkceCodeVerifier,
}

impl SecurityDetails {
    fn new(
        csrf_token: CsrfToken,
        nonce: Nonce,
        pkce_code_verifier: PkceCodeVerifier,
    ) -> Result<SecurityDetails> {
        Ok(SecurityDetails {
            csrf_token,
            nonce,
            pkce_code_verifier,
        })
    }
}
