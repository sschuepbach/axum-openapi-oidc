use std::sync::Arc;

use anyhow::Result;
use axum::routing::{get, post};
use axum::Router;
use cookie::Key;
use utoipa::{
    openapi::security::{ApiKey, ApiKeyValue, SecurityScheme},
    Modify, OpenApi,
};
use utoipa_swagger_ui::SwaggerUi;

use crate::{oidc::OIDCClient, GlobalSettings};

#[derive(OpenApi)]
#[openapi(
        paths(
            handlers::about,
            handlers::login,
            handlers::secured,
            handlers::auth,
        ),
        components(
            // schemas(todo::Todo, todo::TodoError)
        ),
        modifiers(&SecurityAddon),
        tags(
            (name = "todo", description = "Todo items management API")
        )
    )]
struct ApiDoc;

struct SecurityAddon;

// TODO: Authenticate via OAuth2
impl Modify for SecurityAddon {
    fn modify(&self, openapi: &mut utoipa::openapi::OpenApi) {
        if let Some(components) = openapi.components.as_mut() {
            components.add_security_scheme(
                "api_key",
                SecurityScheme::ApiKey(ApiKey::Header(ApiKeyValue::new("todo_apikey"))),
            )
        }
    }
}

pub(crate) struct AppState {
    pub(crate) oidc_client: OIDCClient,
    pub(crate) scopes: &'static [&'static str],
    pub(crate) cookie_key: Key,
}

pub(crate) fn routes<'a>(settings: GlobalSettings) -> Result<Router> {
    let state = Arc::new(AppState {
        oidc_client: OIDCClient::new(
            settings.issuer_url,
            settings.client_id,
            None,
            settings.redirect_url,
        )?,
        scopes: settings.scopes,
        cookie_key: Key::generate(),
    });

    let openapi_route =
        SwaggerUi::new("/api/v1/openapi").url("/api/v1/openapi.json", ApiDoc::openapi());

    Ok(Router::new()
        .route("/about", get(handlers::about))
        .route("/secured", get(handlers::secured))
        .route("/oidc/login", get(handlers::login))
        .route("/oidc/authorize", get(handlers::auth))
        .route("/oidc/refresh_token", get(handlers::refresh))
        .route("/api-token", post(handlers::create_api_token))
        .merge(openapi_route)
        .with_state(state))
}

mod handlers {

    use std::collections::HashMap;

    use crate::errors::AppError;

    use super::*;
    use anyhow::anyhow;
    use axum::{
        extract::{Query, State},
        headers::{authorization::Bearer, Authorization},
        response::Redirect,
        TypedHeader,
    };
    use tower_cookies::{cookie::SameSite, Cookie, Cookies};

    #[utoipa::path(
        get,
        path = "/api/v1/about",
        responses(
            (status = 200, description = "A simple about string", body = String),
        )
    )]
    pub(crate) async fn about() -> String {
        "This is all about the about page".to_string()
    }

    #[utoipa::path(
        get,
        path = "/api/v1/user/login",
        responses(
            (status = 307, description = "Successfully redirected client to OpenID provider"),
            (status = 500, description = "", body = String)
        )
    )]
    pub(crate) async fn login(
        State(state): State<Arc<AppState>>,
        cookies: Cookies,
    ) -> Result<Redirect, AppError> {
        match state.oidc_client.authorization_url(&state.scopes) {
            Ok((url, cookie_value)) => {
                let private_cookies = cookies.private(&state.cookie_key);
                // As the security tokens are sensitive, it is encrypted before saved in browser
                private_cookies.add(Cookie::new("verification".to_string(), cookie_value));
                Ok(Redirect::temporary(url.as_ref()))
            }
            Err(e) => Err(e.into()),
        }
    }

    #[utoipa::path(
        get,
        path = "/api/v1/user/auth",
        responses(
            (status = 200, description = "Todo item created successfully", body = Todo),
            (status = 500, description = "Serialization error", body = String),
            (status = 500, description = "Deserialization error", body = String),
    ))]
    pub(crate) async fn auth(
        State(state): State<Arc<AppState>>,
        Query(params): Query<HashMap<String, String>>,
        cookies: Cookies,
    ) -> Result<(), AppError> {
        let authorization_code = params
            .get("code")
            .ok_or(anyhow!("Missing `code` param in request"))?;
        let state_param = params
            .get("state")
            .ok_or(anyhow!("Missing `state` param in request!"))?;
        let cookie_value = cookies
            .get("verification")
            .ok_or(anyhow!("verification cookie not found"))?;
        let cookie_value = cookie_value.value();
        let private_cookies = cookies.private(&state.cookie_key);
        private_cookies.remove(Cookie::new("verification".to_string(), ""));
        let access_token =
            state
                .oidc_client
                .access_token(authorization_code, cookie_value, state_param)?;
        let mut cookie = Cookie::new("access-token", access_token);
        cookie.set_same_site(SameSite::Strict);
        cookie.set_secure(Some(true));
        cookie.set_http_only(Some(true));

        // As the access token is sensitive, it is encrypted before saved browser
        private_cookies.add(cookie);
        Ok(())
    }

    #[utoipa::path(
        get,
        path = "/api/v1/secured",
        responses(
            (status = 200, description = "Access granted"),
            (status = 403, description = "Access denied")
        )
    )]
    pub(crate) async fn secured(
        TypedHeader(Authorization(bearer)): TypedHeader<Authorization<Bearer>>,
    ) {
        println!("{}", bearer.token());
    }

    pub(crate) async fn refresh() {
        todo!()
    }

    pub(crate) async fn create_api_token() {
        todo!()
    }
}
