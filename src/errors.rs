use axum::{http::StatusCode, response::IntoResponse};

/// A 401 error
#[derive(Debug)]
pub struct UnauthorizedError(anyhow::Error);

impl<T> From<T> for UnauthorizedError
where
    T: Into<anyhow::Error> + Send,
{
    fn from(item: T) -> Self {
        let e: anyhow::Error = item.into();
        Self(e)
    }
}

impl IntoResponse for UnauthorizedError {
    fn into_response(self) -> axum::response::Response {
        (StatusCode::UNAUTHORIZED, self.0.to_string()).into_response()
    }
}

/// An internal server error
#[derive(Debug)]
pub struct AppError(anyhow::Error);

impl<T> From<T> for AppError
where
    T: Into<anyhow::Error> + Send,
{
    fn from(item: T) -> Self {
        let e: anyhow::Error = item.into();
        Self(e)
    }
}

impl IntoResponse for AppError {
    fn into_response(self) -> axum::response::Response {
        (StatusCode::INTERNAL_SERVER_ERROR, self.0.to_string()).into_response()
    }
}
