mod api;
mod errors;
mod oidc;

use anyhow::Error;
use axum::Router;
use tower_cookies::CookieManagerLayer;
use tower_http::{cors::CorsLayer, services::ServeDir};

#[derive(Clone, Copy, Debug)]
struct GlobalSettings {
    issuer_url: &'static str,
    client_id: &'static str,
    redirect_url: &'static str,
    server_address: &'static str,
    scopes: &'static [&'static str],
}

impl Default for GlobalSettings {
    fn default() -> Self {
        let issuer_url = "https://auth.ub.unibas.ch/realms/test";
        let client_id = "my-test-client";
        let redirect_url = "https://oidc-client.memobase.k8s.unibas.ch/api/v1/oidc/authorize";
        let server_address = "0.0.0.0:3000";
        let scopes = &["email", "roles"];
        Self {
            issuer_url,
            client_id,
            redirect_url,
            server_address,
            scopes,
        }
    }
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let global_settings = GlobalSettings::default();
    let app = Router::new()
        .nest_service(
            "/",
            ServeDir::new("ui/").append_index_html_on_directories(true),
        )
        .nest("/api/v1", api::v1::routes(global_settings)?)
        .layer(CookieManagerLayer::new())
        // TODO: Add gradually more restrictions
        .layer(CorsLayer::very_permissive());

    axum::Server::bind(&global_settings.server_address.parse()?)
        .serve(app.into_make_service())
        .await?;

    Ok(())
}
