/** @type {import('./$types').PageLoad} */
export function load({ params }) {
    content: {
        about: fetch('https://oidc-client.memobase.k8s.unibas.ch/about')
    }
}
