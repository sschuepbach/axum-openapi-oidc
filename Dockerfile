FROM scratch
WORKDIR /app
COPY backend/oidc-client app
COPY frontend ui
ENTRYPOINT ["./app"]
